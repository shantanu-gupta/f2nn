#!/usr/bin/env python

import os
import subprocess
import datetime
import sys
import concurrent.futures

def run_fisher(fpath, fisher_params, gmm_params_list, enc_path):
    subprocess.call(['./bin/fisher', fpath, fisher_params,
                     *gmm_params_list, enc_path])

def main():
    imgs_listfile = sys.argv[1]
    dataset_rootdir = sys.argv[2]
    fisher_params_file = sys.argv[3]
    gmm_means = sys.argv[4]
    gmm_covs = sys.argv[5]
    gmm_priors = sys.argv[6]
    encoding_rootdir = sys.argv[7]
    gmm_params_list = [gmm_means, gmm_covs, gmm_priors]

    with open(imgs_listfile) as f:
        files_list = [line.strip() for line in f.readlines()
                                   if len(line.strip())]

    N = len(files_list)
    checkpoints = [i * (N // 100) for i in range(1, 101)]
    if checkpoints[-1] != N:
        checkpoints.append(N)

    enc_paths = []
    for i, fpath in enumerate(files_list):
        enc_path = fpath.replace(dataset_rootdir, encoding_rootdir)
        enc_paths.append(enc_path)

        enc_dir = os.path.dirname(enc_path)
        if not os.path.exists(enc_dir):
            os.makedirs(enc_dir)

    gmm_params_lists = [gmm_params_list] * N
    fisher_params_lists = [fisher_params_file] * N
    chunksize = max(N // 100, 1)
    with concurrent.futures.ProcessPoolExecutor() as executor:
        i = 0
        for result in executor.map(run_fisher,
                                    files_list, fisher_params_lists,
                                    gmm_params_lists, enc_paths,
                                    chunksize=chunksize):
            i += 1
            if i in checkpoints:
                now = datetime.datetime.now()
                print('{} - {}/{} ({:.2f}%)'.format(now, i, N, (i * 100) / N))

if __name__ == '__main__':
    main()

#!/usr/bin/env python

import os
import subprocess
import datetime
import sys
import concurrent.futures

def run_dsift(fpath, desc_path, params_file):
    subprocess.call(['./bin/dsift', fpath, desc_path, params_file])

def main():
    imgs_listfile = sys.argv[1]
    sift_params_file = sys.argv[2]
    dataset_rootdir = sys.argv[3]
    descriptors_rootdir = sys.argv[4]
    with open(imgs_listfile) as f:
        files_list = [line.strip() for line in f.readlines()
                                   if len(line.strip())]
    img_file_format = os.path.splitext(files_list[0])[1]

    N = len(files_list)
    checkpoints = [i * (N // 100) for i in range(1, 101)]
    if checkpoints[-1] != N:
        checkpoints.append(N)

    desc_paths = []
    params_files = []
    for i, fpath in enumerate(files_list):
        desc_path = fpath.replace(dataset_rootdir, descriptors_rootdir)\
                        .replace(img_file_format, '.csv')
        desc_paths.append(desc_path)

        desc_dir = os.path.dirname(desc_path)
        if not os.path.exists(desc_dir):
            print('Making directory {}.'.format(desc_dir))
            os.makedirs(desc_dir)

        params_files.append(sift_params_file)

    chunksize = max(N // 100, 1)
    with concurrent.futures.ProcessPoolExecutor() as executor:
        i = 0
        for result in executor.map(run_dsift, files_list, desc_paths,
                                    params_files, chunksize=chunksize):
            i += 1
            if i in checkpoints:
                now = datetime.datetime.now()
                print('{} - {}/{} ({:.2f}%)'.format(now, i, N, (i * 100) / N))

if __name__ == '__main__':
    main()

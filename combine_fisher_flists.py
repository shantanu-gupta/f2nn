#!/usr/bin/env python

import os
import sys
import subprocess

import argparse

def get_key_from_line(line):
    fname = os.path.splitext(os.path.basename(line))[0]
    try:
        return int(fname.split('_')[0])
    except ValueError:
        return fname.split('_')[0]

CROPS = ['1x1_0',
         '1x3_0', '1x3_1', '1x3_2',
         '2x2_0', '2x2_1', '2x2_2', '2x2_3',
         '3x1_0', '3x1_1', '3x1_2']

def main():
    description = ('Concatenate Fisher vectors from different crops of a given '
                    'dataset.')
    basedir_help = ('Base directory containing the listfiles of different '
                    'crops.')
    dataset_help = ('Name of the dataset.')

    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-d', '--base-dir', nargs=1, help=basedir_help)
    parser.add_argument('-f', '--dataset', nargs=1, help=dataset_help)

    args = parser.parse_args()
    basedir, = args.base_dir
    dataset, = args.dataset

    fpaths = [os.path.join(basedir,
                           'fisher-crop_{}-{}-files.txt'.format(crop, dataset))
              for crop in CROPS]
    sorted_lines = []
    for path in fpaths:
        with open(path) as f:
            sorted_lines.append(sorted(map(str.strip, f.readlines()),
                                       key=get_key_from_line))
    sorted_lines = zip(*sorted_lines)
    for i, combo in enumerate(sorted_lines):
        output_filename = combo[0].replace('_1x1_0', '')
        if os.path.exists(output_filename):
            print('Output file {} already exists. Exiting.'\
                        .format(output_filename))
            sys.exit(1)

        with open(output_filename, 'w') as out:
            subprocess.call(['cat', *combo], stdout=out)
        print(output_filename)

if __name__ == '__main__':
    main()

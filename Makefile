SRC						:= src
BIN						:= bin
INTERMEDIATE_DATA		:= idata
METADATA				:= metadata
MODELS					:= models
TMP_DIR					:= /dev/shm
BENCHMARK				:= benchmark/classify_output

# Set up the training process.
# DATASET					:= caltech-101
# DATASET_DIR				:= /home/shantanu/data/caltech-101/101_ObjectCategories
# IMAGE_FORMAT			:= "jpg"
# DATASET					:= caltech-1
# DATASET_DIR				:= /home/shantanu/data/caltech-101/1_ObjectCategories
# IMAGE_FORMAT			:= "jpg"
# DATASET					:= tiny-imagenet-200
# DATASET_DIR				:= /home/shantanu/data/tiny-imagenet-200/train
# IMAGE_FORMAT			:= "JPEG"
# DATASET					:= pascal-voc-2012
# DATASET_DIR				:= /home/shantanu/data/VOCtrainval_11-May-2012/VOCdevkit/VOC2012/JPEGImages
# DATASET_DIR				:= /home/shantanu/data/VOCtrainval_11-May-2012/VOCdevkit/VOC2012/JPEGImages_100
# IMAGE_FORMAT			:= "jpg"
DATASET					:= cifar-10
DATASET_DIR				:= /home/shantanu/data/cifar-10/train
IMAGE_FORMAT			:= "png"
ORIGINAL_FILELIST		:= $(METADATA)/$(DATASET)-files.txt

TEMPLATE_DATASET		 = $(DATASET)-template
TEMPLATE_DATASET_DIR	 = $(DATASET_DIR)
TEMPLATE_IMAGE_FORMAT	 = $(IMAGE_FORMAT)

TEST_DATASET			 = cifar-10-test
TEST_DATASET_DIR		 = /home/shantanu/data/cifar-10/test
TEST_IMAGE_FORMAT		 = $(IMAGE_FORMAT)

PARAMS_DIR				:= params
SIFT_PARAMS_FILE		:= $(PARAMS_DIR)/$(DATASET)/dsift.json
FISHER_PARAMS_FILE		:= $(PARAMS_DIR)/$(DATASET)/fisher.json

IMAGE_SEARCH_TERM		:= "*.$(IMAGE_FORMAT)"
SPP_CROPS				:= 1x1_0 \
							1x3_0 1x3_1 1x3_2 \
							2x2_0 2x2_1 2x2_2 2x2_3 \
							3x1_0 3x1_1 3x1_2
CROPS_IMAGE_SEARCH_TERM	 = "*_$*.$(IMAGE_FORMAT)"
CROPS_DIR				:= $(INTERMEDIATE_DATA)/crops-$(DATASET)
CROPS_FILELIST	 		 = $(METADATA)/crop_%-$(DATASET)-files.txt
CROPS_CHECKPOINT		:= $(METADATA)/checkpoint-crops-$(DATASET)

DSIFT_SEARCH_TERM		 = "*_$*.csv"
DSIFT_DIR				 = $(INTERMEDIATE_DATA)/dsift-$(DATASET)
DSIFT_FILELIST			 = $(METADATA)/dsift-crop_%-$(DATASET)-files.txt
DSIFT_CHECKPOINT		 = $(METADATA)/checkpoint-dsift-$(DATASET)

DSIFT_PCA_SEARCH_TERM	 = $(DSIFT_SEARCH_TERM)
DSIFT_PCA_DIR			 = $(INTERMEDIATE_DATA)/pca-dsift-$(DATASET)
DSIFT_PCA_FILELIST		 = $(METADATA)/pca-dsift-crop_%-$(DATASET)-files.txt
# Not a target.
DSIFT_PCA_MODEL			 = $(MODELS)/$(DATASET)/pca-model-dsift-crop_$*.pickle
DSIFT_PCA_CHECKPOINT	 = $(METADATA)/checkpoint-pca-dsift-$(DATASET)

GMM_DIR					 = $(MODELS)/$(DATASET)/gmm-pca-dsift/$*
GMM_MEANS				 = $(GMM_DIR)/means.csv
GMM_COVS				 = $(GMM_DIR)/covs.csv
GMM_PRIORS				 = $(GMM_DIR)/priors.csv
GMM_CROP_CHECKPOINT		 = $(METADATA)/checkpoint-gmm-pca-dsift-crop_%-$(DATASET)
GMM_CHECKPOINT			 = $(METADATA)/checkpoint-gmm-pca-dsift-$(DATASET)

FISHER_SEARCH_TERM		 = $(DSIFT_SEARCH_TERM)
FISHER_DIR				 = $(INTERMEDIATE_DATA)/fisher-$(DATASET)
FISHER_FILELIST			 = $(METADATA)/fisher-crop_%-$(DATASET)-files.txt
FISHER_CHECKPOINT		 = $(METADATA)/checkpoint-fisher-$(DATASET)

# Set up the benchmark
TEMPLATE_IMAGE_SEARCH_TERM = "*.$(TEMPLATE_IMAGE_FORMAT)"
TEMPLATE_FILELIST		 = $(METADATA)/$(TEMPLATE_DATASET)-files.txt
TEMPLATE_CROPS_IMAGE_SEARCH_TERM = "*_$*.$(TEMPLATE_IMAGE_FORMAT)"
TEMPLATE_CROPS_DIR		 = $(INTERMEDIATE_DATA)/crops-$(TEMPLATE_DATASET)
TEMPLATE_CROPS_FILELIST	 = $(METADATA)/crop_%-$(TEMPLATE_DATASET)-files.txt
TEMPLATE_CROPS_CHECKPOINT= $(METADATA)/checkpoint-crops-$(TEMPLATE_DATASET)

TEMPLATE_DSIFT_DIR		 = $(INTERMEDIATE_DATA)/dsift-$(TEMPLATE_DATASET)
TEMPLATE_DSIFT_FILELIST	 = $(METADATA)/dsift-crop_%-$(TEMPLATE_DATASET)-files.txt
TEMPLATE_DSIFT_CHECKPOINT= $(METADATA)/checkpoint-dsift-$(TEMPLATE_DATASET)

TEMPLATE_DSIFT_PCA_DIR	 = $(INTERMEDIATE_DATA)/pca-dsift-$(TEMPLATE_DATASET)
TEMPLATE_DSIFT_PCA_FILELIST = $(METADATA)/pca-dsift-crop_%-$(TEMPLATE_DATASET)-files.txt
TEMPLATE_DSIFT_PCA_CHECKPOINT = $(METADATA)/checkpoint-pca-dsift-$(TEMPLATE_DATASET)

TEMPLATE_FISHER_DIR		 = $(INTERMEDIATE_DATA)/fisher-$(TEMPLATE_DATASET)
TEMPLATE_FISHER_FILELIST = $(METADATA)/fisher-crop_%-$(TEMPLATE_DATASET)-files.txt
TEMPLATE_FISHER_CHECKPOINT = $(METADATA)/checkpoint-fisher-$(TEMPLATE_DATASET)

# Filelist combining locations of FVs computed from all crops
TEMPLATE_FISHER_COMBINED_FILELIST	= $(METADATA)/fisher-$(TEMPLATE_DATASET)-files.txt

# And the test for the benchmark
TEST_IMAGE_SEARCH_TERM	 = "*.$(TEST_IMAGE_FORMAT)"
TEST_FILELIST			 = $(METADATA)/$(TEST_DATASET)-files.txt
TEST_CROPS_IMAGE_SEARCH_TERM = "*_$*.$(TEST_IMAGE_FORMAT)"
TEST_CROPS_DIR			 = $(INTERMEDIATE_DATA)/crops-$(TEST_DATASET)
TEST_CROPS_FILELIST		 = $(METADATA)/crop_%-$(TEST_DATASET)-files.txt
TEST_CROPS_CHECKPOINT	 = $(METADATA)/checkpoint-crops-$(TEST_DATASET)

TEST_DSIFT_DIR			 = $(INTERMEDIATE_DATA)/dsift-$(TEST_DATASET)
TEST_DSIFT_FILELIST		 = $(METADATA)/dsift-crop_%-$(TEST_DATASET)-files.txt
TEST_DSIFT_CHECKPOINT	 = $(METADATA)/checkpoint-dsift-$(TEST_DATASET)

TEST_DSIFT_PCA_DIR		 = $(INTERMEDIATE_DATA)/pca-dsift-$(TEST_DATASET)
TEST_DSIFT_PCA_FILELIST	 = $(METADATA)/pca-dsift-crop_%-$(TEST_DATASET)-files.txt
TEST_DSIFT_PCA_CHECKPOINT= $(METADATA)/checkpoint-pca-dsift-$(TEST_DATASET)

TEST_FISHER_DIR			 = $(INTERMEDIATE_DATA)/fisher-$(TEST_DATASET)
TEST_FISHER_FILELIST	 = $(METADATA)/fisher-crop_%-$(TEST_DATASET)-files.txt
TEST_FISHER_CHECKPOINT 	 = $(METADATA)/checkpoint-fisher-$(TEST_DATASET)

TEST_FISHER_COMBINED_FILELIST		= $(METADATA)/fisher-$(TEST_DATASET)-files.txt

# No recipe for these two yet. Have to make them manually.
BENCHMARK_TEMPLATE_PAIRS_FILE = $(METADATA)/fisher-$(TEMPLATE_DATASET)-files-with-labels.txt
BENCHMARK_TEST_PAIRS_FILE = $(METADATA)/fisher-$(TEST_DATASET)-files-with-labels.txt

# Finally!
BENCHMARK_OUTPUT_FILE	 = $(BENCHMARK)/$(TEST_DATASET)-with-$(TEMPLATE_DATASET)-result.txt

################################################################################
# Common stuff about building the code, etc.
CXX						:= g++
CXXFLAGS 				:= -Wall -Wextra -O2
LDFLAGS 				:= -L$(VLROOT)/bin/glnxa64 \
							-lopencv_core -lopencv_imgcodecs -lopencv_highgui \
							-lopencv_ml -lopencv_imgproc \
							-lvl
JSON_NLOHMANN			:= /opt/json-develop/single_include
INCLUDE 				:= -I$(VLROOT) -I$(JSON_NLOHMANN)

################################################################################
################################################################################
all: binaries

clean:
	trash $(BIN)/* $(METADATA)/*

nuke:
	trash $(BIN)/* $(METADATA)/* $(INTERMEDIATE_DATA)/* $(MODELS)/*

binaries: $(BIN)/dsift $(BIN)/train_gmm $(BIN)/fisher

$(BIN)/%: $(SRC)/%.cpp
	$(CXX) $(CXXFLAGS) $< $(INCLUDE) $(LDFLAGS) -o $@

.PHONY: all clean binaries nuke

.PRECIOUS: $(METADATA)/crop_%-$(DATASET)-files.txt \
			$(METADATA)/checkpoint-crops-$(DATASET) \
			$(METADATA)/dsift-crop_%-$(DATASET)-files.txt \
			$(METADATA)/checkpoint-dsift-$(DATASET) \
			$(METADATA)/pca-dsift-crop_%-$(DATASET)-files.txt \
			$(METADATA)/checkpoint-pca-dsift-$(DATASET) \
			$(METADATA)/checkpoint-gmm-pca-dsift-crop_%-$(DATASET) \
			$(METADATA)/checkpoint-gmm-pca-dsift-$(DATASET) \
			$(METADATA)/fisher-gmm-pca-dsift-crop_%-$(DATASET)-files.txt \
			$(METADATA)/checkpoint-fisher-gmm-pca-dsift-$(DATASET) \
# Template files \
			$(METADATA)/crop_%-$(TEMPLATE_DATASET)-files.txt \
			$(METADATA)/checkpoint-crops-$(TEMPLATE_DATASET) \
			$(METADATA)/dsift-crop_%-$(TEMPLATE_DATASET)-files.txt \
			$(METADATA)/checkpoint-dsift-$(TEMPLATE_DATASET) \
			$(METADATA)/pca-dsift-crop_%-$(TEMPLATE_DATASET)-files.txt \
			$(METADATA)/checkpoint-pca-dsift-$(TEMPLATE_DATASET) \
			$(METADATA)/fisher-gmm-pca-dsift-crop_%-$(TEMPLATE_DATASET)-files.txt \
			$(METADATA)/checkpoint-fisher-gmm-pca-dsift-$(TEMPLATE_DATASET) \
			$(METADATA)/fisher-$(TEMPLATE_DATASET)-files.txt \
# Test files \
			$(METADATA)/crop_%-$(TEST_DATASET)-files.txt \
			$(METADATA)/checkpoint-crops-$(TEST_DATASET) \
			$(METADATA)/dsift-crop_%-$(TEST_DATASET)-files.txt \
			$(METADATA)/checkpoint-dsift-$(TEST_DATASET) \
			$(METADATA)/pca-dsift-crop_%-$(TEST_DATASET)-files.txt \
			$(METADATA)/checkpoint-pca-dsift-$(TEST_DATASET) \
			$(METADATA)/fisher-gmm-pca-dsift-crop_%-$(TEST_DATASET)-files.txt \
			$(METADATA)/checkpoint-fisher-gmm-pca-dsift-$(TEST_DATASET) \
			$(METADATA)/fisher-$(TEST_DATASET)-files.txt
################################################################################
################################################################################
$(ORIGINAL_FILELIST): ./generate_filelist.sh
	./generate_filelist.sh $(IMAGE_SEARCH_TERM) $(DATASET_DIR) $@

################################################################################
# This checkpoint ensures that tile-cropping is done only once even though we
# want to generate multiple listfiles later, one for each crop type.
$(CROPS_CHECKPOINT): $(ORIGINAL_FILELIST) ./tile_crop_imgs.py
	./tile_crop_imgs.py $(ORIGINAL_FILELIST) $(DATASET_DIR) $(CROPS_DIR)
	touch $@

$(CROPS_FILELIST): $(CROPS_CHECKPOINT) ./generate_filelist.sh
	./generate_filelist.sh $(CROPS_IMAGE_SEARCH_TERM) $(CROPS_DIR) $@

################################################################################
# Ok to perform each DSIFT individually here. No need for an intermediate
# checkpoint.
$(DSIFT_FILELIST): $(CROPS_FILELIST) ./get_diff_root_listfile.py \
					$(BIN)/dsift ./extract_signatures.py $(SIFT_PARAMS_FILE) \
					./generate_filelist.sh
	./get_diff_root_listfile.py -f $< -o $(TMP_DIR)/list_$* \
								-s $(CROPS_DIR) -r $(DSIFT_DIR) \
								-e ".csv"
	paste $< $(TMP_DIR)/list_$* > $(TMP_DIR)/pairs_$*
	./extract_signatures.py -p $(TMP_DIR)/pairs_$* dsift $(BIN)/dsift $(SIFT_PARAMS_FILE)
	./generate_filelist.sh $(DSIFT_SEARCH_TERM) $(DSIFT_DIR) $@
	trash $(TMP_DIR)/list_$* $(TMP_DIR)/pairs_$*

# This checkpoint only present for debugging the Makefile, really.
$(DSIFT_CHECKPOINT): $(foreach crop, $(SPP_CROPS), \
						$(METADATA)/dsift-crop_$(crop)-$(DATASET)-files.txt)
	touch $@

################################################################################
# Add PCA step to DSIFT.
$(DSIFT_PCA_FILELIST): $(DSIFT_FILELIST) ./get_pca_features.py ./generate_filelist.sh $(FISHER_PARAMS_FILE)
	./get_diff_root_listfile.py -f $< -o $(TMP_DIR)/list_pca_$* \
								-s $(DSIFT_DIR) -r $(DSIFT_PCA_DIR)
	paste $< $(TMP_DIR)/list_pca_$* > $(TMP_DIR)/pairs_pca_$*
	./get_pca_features.py -p $(TMP_DIR)/pairs_pca_$* -m $(DSIFT_PCA_MODEL) \
							-t $(FISHER_PARAMS_FILE) --train
	./generate_filelist.sh $(DSIFT_PCA_SEARCH_TERM) $(DSIFT_PCA_DIR) $@
	trash $(TMP_DIR)/list_pca_$* $(TMP_DIR)/pairs_pca_$*

# Another debug checkpoint
$(DSIFT_PCA_CHECKPOINT): $(foreach crop, $(SPP_CROPS), \
							$(METADATA)/pca-dsift-crop_$(crop)-$(DATASET)-files.txt)
	touch $@

################################################################################
# Adding a checkpoint here as we generate 3 files here. Tracking all 3 together
# with Make is ... tricky, it looks like.
# Otherwise this is the same sort of step as the previous 2.
$(GMM_CROP_CHECKPOINT): $(BIN)/train_gmm $(DSIFT_PCA_FILELIST) $(FISHER_PARAMS_FILE)
	mkdir -p $(GMM_DIR)
	$^ $(GMM_MEANS) $(GMM_COVS) $(GMM_PRIORS)
	touch $@

# Yet another debug checkpoint
$(GMM_CHECKPOINT): $(foreach crop, $(SPP_CROPS), \
						$(METADATA)/checkpoint-gmm-pca-dsift-crop_$(crop)-$(DATASET))
	touch $@

################################################################################
$(FISHER_FILELIST): $(DSIFT_PCA_FILELIST) ./get_diff_root_listfile.py \
					$(GMM_CROP_CHECKPOINT) ./generate_filelist.sh \
					$(BIN)/fisher ./extract_signatures.py $(FISHER_PARAMS_FILE)
	./get_diff_root_listfile.py -f $< -o $(TMP_DIR)/list_fisher_$* \
								-s $(DSIFT_PCA_DIR) -r $(FISHER_DIR)
	paste $< $(TMP_DIR)/list_fisher_$* > $(TMP_DIR)/pairs_fisher_$*
	./extract_signatures.py -p $(TMP_DIR)/pairs_fisher_$* \
							fisher $(BIN)/fisher $(FISHER_PARAMS_FILE) $(GMM_DIR)
	./generate_filelist.sh $(FISHER_SEARCH_TERM) $(FISHER_DIR) $@
	trash $(TMP_DIR)/list_fisher_$* $(TMP_DIR)/pairs_fisher_$*

# Final thing being produced, so this checkpoint is how we do it.
$(FISHER_CHECKPOINT): $(foreach crop, $(SPP_CROPS), \
						$(METADATA)/fisher-crop_$(crop)-$(DATASET)-files.txt)
	touch $@
	
################################################################################
# Same process for template as for training, but now with a trained model
# instead of computing one.
$(TEMPLATE_FILELIST): ./generate_filelist.sh
	./generate_filelist.sh $(TEMPLATE_IMAGE_SEARCH_TERM) $(TEMPLATE_DATASET_DIR) $@

$(TEMPLATE_CROPS_CHECKPOINT): $(TEMPLATE_FILELIST) ./tile_crop_imgs.py
	./tile_crop_imgs.py $(TEMPLATE_FILELIST) $(TEMPLATE_DATASET_DIR) $(TEMPLATE_CROPS_DIR)
	touch $@

$(TEMPLATE_CROPS_FILELIST): $(TEMPLATE_CROPS_CHECKPOINT) ./generate_filelist.sh
	./generate_filelist.sh $(TEMPLATE_CROPS_IMAGE_SEARCH_TERM) $(TEMPLATE_CROPS_DIR) $@

$(TEMPLATE_DSIFT_FILELIST): $(TEMPLATE_CROPS_FILELIST) ./get_diff_root_listfile.py \
					$(BIN)/dsift ./extract_signatures.py $(SIFT_PARAMS_FILE) \
					./generate_filelist.sh
	./get_diff_root_listfile.py -f $< -o $(TMP_DIR)/list_$* \
								-s $(TEMPLATE_CROPS_DIR) -r $(TEMPLATE_DSIFT_DIR) \
								-e ".csv"
	paste $< $(TMP_DIR)/list_$* > $(TMP_DIR)/pairs_$*
	./extract_signatures.py -p $(TMP_DIR)/pairs_$* dsift $(BIN)/dsift $(SIFT_PARAMS_FILE)
	./generate_filelist.sh $(DSIFT_SEARCH_TERM) $(TEMPLATE_DSIFT_DIR) $@
	trash $(TMP_DIR)/list_$* $(TMP_DIR)/pairs_$*

$(TEMPLATE_DSIFT_CHECKPOINT): $(foreach crop, $(SPP_CROPS), \
						$(METADATA)/dsift-crop_$(crop)-$(TEMPLATE_DATASET)-files.txt)
	touch $@

# Remove the --train flag when doing PCA.
$(TEMPLATE_DSIFT_PCA_FILELIST): $(TEMPLATE_DSIFT_FILELIST) ./get_pca_features.py \
								./generate_filelist.sh $(FISHER_PARAMS_FILE)
	./get_diff_root_listfile.py -f $< -o $(TMP_DIR)/list_pca_$* \
								-s $(TEMPLATE_DSIFT_DIR) -r $(TEMPLATE_DSIFT_PCA_DIR)
	paste $< $(TMP_DIR)/list_pca_$* > $(TMP_DIR)/pairs_pca_$*
	./get_pca_features.py -p $(TMP_DIR)/pairs_pca_$* -m $(DSIFT_PCA_MODEL) \
							-t $(FISHER_PARAMS_FILE)
	./generate_filelist.sh $(DSIFT_PCA_SEARCH_TERM) $(TEMPLATE_DSIFT_PCA_DIR) $@
	trash $(TMP_DIR)/list_pca_$* $(TMP_DIR)/pairs_pca_$*

$(TEMPLATE_DSIFT_PCA_CHECKPOINT): $(foreach crop, $(SPP_CROPS), \
							$(METADATA)/pca-dsift-crop_$(crop)-$(TEMPLATE_DATASET)-files.txt)
	touch $@

# No GMM training step; we use the model already trained on the original
# dataset.
$(TEMPLATE_FISHER_FILELIST): $(TEMPLATE_DSIFT_PCA_FILELIST) ./get_diff_root_listfile.py \
					$(GMM_CROP_CHECKPOINT) ./generate_filelist.sh \
					$(BIN)/fisher ./extract_signatures.py $(FISHER_PARAMS_FILE)
	./get_diff_root_listfile.py -f $< -o $(TMP_DIR)/list_fisher_$* \
								-s $(TEMPLATE_DSIFT_PCA_DIR) -r $(TEMPLATE_FISHER_DIR)
	paste $< $(TMP_DIR)/list_fisher_$* > $(TMP_DIR)/pairs_fisher_$*
	./extract_signatures.py -p $(TMP_DIR)/pairs_fisher_$* \
							fisher $(BIN)/fisher $(FISHER_PARAMS_FILE) $(GMM_DIR)
	./generate_filelist.sh $(FISHER_SEARCH_TERM) $(TEMPLATE_FISHER_DIR) $@
	trash $(TMP_DIR)/list_fisher_$* $(TMP_DIR)/pairs_fisher_$*

$(TEMPLATE_FISHER_CHECKPOINT): $(foreach crop, $(SPP_CROPS), \
						$(METADATA)/fisher-crop_$(crop)-$(TEMPLATE_DATASET)-files.txt)
	touch $@

$(TEMPLATE_FISHER_COMBINED_FILELIST): $(TEMPLATE_FISHER_CHECKPOINT) ./combine_fisher_flists.py
	./combine_fisher_flists.py -d $(METADATA) -f $(TEMPLATE_DATASET) > $@

################################################################################
# Exact same process for test...
$(TEST_FILELIST): ./generate_filelist.sh
	./generate_filelist.sh $(TEST_IMAGE_SEARCH_TERM) $(TEST_DATASET_DIR) $@

$(TEST_CROPS_CHECKPOINT): $(TEST_FILELIST) ./tile_crop_imgs.py
	./tile_crop_imgs.py $(TEST_FILELIST) $(TEST_DATASET_DIR) $(TEST_CROPS_DIR)
	touch $@

$(TEST_CROPS_FILELIST): $(TEST_CROPS_CHECKPOINT) ./generate_filelist.sh
	./generate_filelist.sh $(TEST_CROPS_IMAGE_SEARCH_TERM) $(TEST_CROPS_DIR) $@

$(TEST_DSIFT_FILELIST): $(TEST_CROPS_FILELIST) ./get_diff_root_listfile.py \
					$(BIN)/dsift ./extract_signatures.py $(SIFT_PARAMS_FILE) \
					./generate_filelist.sh
	./get_diff_root_listfile.py -f $< -o $(TMP_DIR)/list_$* \
								-s $(TEST_CROPS_DIR) -r $(TEST_DSIFT_DIR) \
								-e ".csv"
	paste $< $(TMP_DIR)/list_$* > $(TMP_DIR)/pairs_$*
	./extract_signatures.py -p $(TMP_DIR)/pairs_$* dsift $(BIN)/dsift $(SIFT_PARAMS_FILE)
	./generate_filelist.sh $(DSIFT_SEARCH_TERM) $(TEST_DSIFT_DIR) $@
	trash $(TMP_DIR)/list_$* $(TMP_DIR)/pairs_$*

$(TEST_DSIFT_CHECKPOINT): $(foreach crop, $(SPP_CROPS), \
						$(METADATA)/dsift-crop_$(crop)-$(TEST_DATASET)-files.txt)
	touch $@

# Remove the --train flag when doing PCA.
$(TEST_DSIFT_PCA_FILELIST): $(TEST_DSIFT_FILELIST) ./get_pca_features.py \
								./generate_filelist.sh $(FISHER_PARAMS_FILE)
	./get_diff_root_listfile.py -f $< -o $(TMP_DIR)/list_pca_$* \
								-s $(TEST_DSIFT_DIR) -r $(TEST_DSIFT_PCA_DIR)
	paste $< $(TMP_DIR)/list_pca_$* > $(TMP_DIR)/pairs_pca_$*
	./get_pca_features.py -p $(TMP_DIR)/pairs_pca_$* -m $(DSIFT_PCA_MODEL) \
							-t $(FISHER_PARAMS_FILE)
	./generate_filelist.sh $(DSIFT_PCA_SEARCH_TERM) $(TEST_DSIFT_PCA_DIR) $@
	trash $(TMP_DIR)/list_pca_$* $(TMP_DIR)/pairs_pca_$*

$(TEST_DSIFT_PCA_CHECKPOINT): $(foreach crop, $(SPP_CROPS), \
							$(METADATA)/pca-dsift-crop_$(crop)-$(TEST_DATASET)-files.txt)
	touch $@

# No GMM training step; we use the model already trained on the original
# dataset.
$(TEST_FISHER_FILELIST): $(TEST_DSIFT_PCA_FILELIST) ./get_diff_root_listfile.py \
					$(GMM_CROP_CHECKPOINT) ./generate_filelist.sh \
					$(BIN)/fisher ./extract_signatures.py $(FISHER_PARAMS_FILE)
	./get_diff_root_listfile.py -f $< -o $(TMP_DIR)/list_fisher_$* \
								-s $(TEST_DSIFT_PCA_DIR) -r $(TEST_FISHER_DIR)
	paste $< $(TMP_DIR)/list_fisher_$* > $(TMP_DIR)/pairs_fisher_$*
	./extract_signatures.py -p $(TMP_DIR)/pairs_fisher_$* \
							fisher $(BIN)/fisher $(FISHER_PARAMS_FILE) $(GMM_DIR)
	./generate_filelist.sh $(FISHER_SEARCH_TERM) $(TEST_FISHER_DIR) $@
	trash $(TMP_DIR)/list_fisher_$* $(TMP_DIR)/pairs_fisher_$*

$(TEST_FISHER_CHECKPOINT): $(foreach crop, $(SPP_CROPS), \
						$(METADATA)/fisher-crop_$(crop)-$(TEST_DATASET)-files.txt)
	touch $@

$(TEST_FISHER_COMBINED_FILELIST): $(TEST_FISHER_CHECKPOINT) ./combine_fisher_flists.py
	./combine_fisher_flists.py -d $(METADATA) -f $(TEST_DATASET) > $@

################################################################################
$(BENCHMARK_OUTPUT_FILE): ./classify_image_signatures.py \
							$(BENCHMARK_TEMPLATE_PAIRS_FILE) $(BENCHMARK_TEST_PAIRS_FILE)
	./classify_image_signatures.py -s $(BENCHMARK_TEMPLATE_PAIRS_FILE) \
									-t $(BENCHMARK_TEST_PAIRS_FILE) \
									-d $(BENCHMARK) \
									-o $@ \
									-m cos -n

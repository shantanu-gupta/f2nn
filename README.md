# What are we trying to do here?

The aim is to demonstrate that a non-convolutional representation can be
finetuned just the same by a fully connected layer which can be trained on a
dataset to learn an overall representation of an image class. This is
demonstrated using pooled Fisher vectors, a SIFT-based encoding which uses a GMM
trained on a set of local descriptors. Once this is done, maybe we can move on
to a different base representation and see where that goes.

# And how do we go about doing this?

We train a Fisher vector representation first, and then train a neural network
embedding on top of it with the same training dataset. That's the first step.

Then we use this representation on a re-identification task with completely
different identities, to show that it works well as a general tool for all
images loosely of the same kind.

For starters, we can use a standard person re-identification task, which has a
few publicly available datasets. At a glance, many images seem to look similar,
so maybe we can learn a representation on one dataset and use it on another, as
a preliminary step. Maybe a little literature survey will help here, to get an
idea of how to train representations for person classification, and what
datasets we can use. Looking at
[this](http://robustsystems.coe.neu.edu/sites/robustsystems.coe.neu.edu/files/systems/projectpages/reiddataset.html),
it looks like VIPeR and CUHK look similar, so this idea should work. They have
the same aspect ratios too, so plain resizing should do the trick here.

Further, in the LPR work at Conduent, we used a pre-trained VGG network and
finetune it on the training corpus after suitable resizing and padding. That
idea should probably work for this task as well. We get some parameters off the
web, finetune the parameters on the training corpus (CUHK), and then run a
re-identification task on VIPeR to get some numbers. The VGG network is trained
on ImageNet, which is probably not possible on this computer. Maybe a subset of
ImageNet can work (TODO: look into this later). For now, we directly train the
Fisher vectors on CUHK and hope for the best. For a fair comparison, we also
train the VGG architecture from scratch on the same dataset, along with a
control architecture (or two, or none...). This looks like a reasonable
experimental setup. The control architectures may not be necessary though, and
we'll probably add more pre-trained CNN models to the experiment once we're done
with VGG.

# What do you need to run this?

## Software
- OpenCV (to read images in when computing Fisher vectors). Also for
  downsampling in SPP.
- ImageMagick (specifically, convert: for cropping)
- VLFeat (to actually compute DSIFT, GMM parameters, and Fisher encodings)
- Caffe (for CNN stuff)
- CUDA (for Caffe)
- Python3 (for various scripts)
- Make, or more generally, build-essential, for gcc, g++, etc.
- virtualenvwrapper is probably going to be useful.

## Datasets
- (Optional) A pre-training dataset like ImageNet
- A training corpus to learn a representation -- CUHK-01/02/03
- A re-identification benchmark dataset similar to the training corpus -- VIPeR

# Things to remember while setting things up (assuming Debian)

- Add OpenCV and VLFeat to PATH and LD\_LIBRARY\_PATH properly.
- Install caffe: install caffe-cuda and maybe build-dep caffe-cuda.
- Install virtualenvwrapper, use toggleglobalsitepackages to enable caffe in
  venvs when using.
- If using bumblebee, then run ipython with optirun to be able to use cuda.
  Same with running caffe from the command line.

# Some internal conventions which are probably good to write down.

## Filename extension names

- **.list** filelist; each line should have just a path in it
- **.lblist** labelled filelist; each line should have a path and a string label,
  with a space separating them
- **.nlblist** numeric labelled filelist; the labels are numeric now
- **.nlbmap** a map describing how the numeric labels in the nlblist file are
  generated

## Directory names

- **src** code
- **bin** generated binaries from C++ code for DSIFT/Fisher encoding
- **data** original data
- **idata** intermediate data obtained by pre-processing
- **metadata** lists of file paths, labels, etc.
- **models** laid out in terms of training dataset used
- **params** (hyper)parameters needed everywhere: DSIFT, GMM, etc.
- **benchmark** results from benchmarking experiments

# TODO

- Batch training GMM.
- Using an actual logger.
- Removing need for toggleglobalsitepackages.
- Add Makefile recipe for generating (training files, label) pairs

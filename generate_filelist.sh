#!/usr/bin/env bash

echo "Listing $1 files from $2 in $3..."
find $2 -type f -name $1 -exec echo {} \; > $3

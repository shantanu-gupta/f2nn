#!/usr/bin/env python

import os
import sys
import subprocess
import datetime
import concurrent.futures

def tile_image(img_path, output_dirpath, n_tilerows, n_tilecols):
    filename, ext = os.path.splitext(os.path.basename(img_path))
    output_filepath = os.path.join(output_dirpath,
                                   '{}_{}x{}_%d{}'.format(filename, 
                                                          n_tilerows,
                                                          n_tilecols,
                                                          ext))
    # See http://www.imagemagick.org/Usage/crop/#crop_equal
    subprocess.call(['convert', img_path,
                     '-crop', '{}x{}@'.format(n_tilecols, n_tilerows),
                     '+repage', '+adjoin',
                     output_filepath])
    return

def generate_all_tiles(fpath, dest_dir):
    tile_image(fpath, dest_dir, 1, 1) # original
    tile_image(fpath, dest_dir, 3, 1)
    tile_image(fpath, dest_dir, 1, 3)
    tile_image(fpath, dest_dir, 2, 2)

def main():
    imgs_listfile = sys.argv[1]
    dataset_rootdir = sys.argv[2]
    crops_rootdir = sys.argv[3]

    with open(imgs_listfile) as f:
        files_list = [line.strip() for line in f.readlines()
                                   if len(line.strip())]

    N = len(files_list)
    checkpoints = [i * (N // 100) for i in range(1, 101)]
    if checkpoints[-1] != N:
        checkpoints.append(N)

    dest_dirs = []
    for i, fpath in enumerate(files_list):
        dest_path = fpath.replace(dataset_rootdir, crops_rootdir)
        dest_dir = os.path.dirname(dest_path)
        dest_dirs.append(dest_dir)
        if not os.path.exists(dest_dir):
            os.makedirs(dest_dir)

    chunksize = max(N // 100, 1)
    with concurrent.futures.ProcessPoolExecutor() as executor:
        i = 0
        for result in executor.map(generate_all_tiles, files_list, dest_dirs,
                                    chunksize=chunksize):
            i += 1
            if i in checkpoints:
                now = datetime.datetime.now()
                print('{} - {}/{} ({:.2f}%)'.format(now, i, N, (i * 100) / N))

if __name__ == '__main__':
    main()

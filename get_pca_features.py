#!/usr/bin/env python
""" get_pca_features.py

    Get reduced-dimensionality features from a feature dataset using PCA.
    If a PCA model is already known then use it, otherwise train it using the
    given dataset.

    PCA is learned incrementally, using sklearn.decomposition.IncrementalPCA.
"""

import os
import sys
import datetime
import pickle
import argparse
import json

import numpy as np
from sklearn.decomposition import IncrementalPCA

N_PCA_DIMS = None

def main():
    description = ('Reduce dimensionality of feature vector datasets using PCA')
    listfile_help = ('A file containing a list of pairs of paths, containing '
                    'the locations of the files which we want to reduce the '
                    'dimension of, and where the corresponding PCA projection '
                    'should go. The file format expected is CSV.')
    train_flag_help = ('If this flag is set, the PCA model will first be '
                        'trained on the given dataset. The parameters will be '
                        'exported for later use if a location is given.')
    model_help = ('Where the PCA model is/will be present. If the --train flag '
                    'is set, then the model will be stored there after '
                    'training. Otherwise the model already present in this '
                    'path will be used.')
    params_help = ('File containing model configuration (hyperparameters): '
                    'the PCA projection dimensions, and whether to whiten '
                    'the projection. Only used if the --train flag is set, '
                    'otherwise the model entered is used as is.')

    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-p', '--paths', nargs=1, help=listfile_help)
    parser.add_argument('-m', '--model', nargs=1, help=model_help)
    parser.add_argument('--train', action='store_true', help=train_flag_help)
    parser.add_argument('-t', '--params', nargs=1, help=params_help)

    print('Parsing the command-line arguments...')
    args = parser.parse_args()

    pairs_listfile, = args.paths
    train_flag = args.train
    model_filename, = args.model
    model_params_filename, = args.params
    
    with open(pairs_listfile) as inp:
        file_paths = [line.split() for line in inp if len(line.strip()) > 0]
    N = len(file_paths)
    checkpoints = [i * (N // 100) for i in range(1, 101)]
    if checkpoints[-1] != N:
        checkpoints.append(N)

    if train_flag:
        print('Reading model hyper-parameters...')
        with open(model_params_filename) as f:
            params = json.load(f)

        print('Fitting PCA...')
        pca = IncrementalPCA(n_components=params['n_pca_dims'],
                             whiten=params['whiten'],
                             copy=False) # batches are removed after
                                         # partial_fit anyway
        batch = None
        for i, (input_path, output_path) in enumerate(file_paths):
            feat = np.loadtxt(input_path, delimiter=',')

            # partial_fit fails with a weird error if feat.shape[0] < N_PCA_DIMS
            # We need to batch up features if we are to get this to work.
            # See https://github.com/scikit-learn/scikit-learn/issues/6452 for more
            # info.
            if feat.shape[0] < params['n_pca_dims']:
                if batch is None:
                    batch = feat
                else:
                    batch = np.vstack((batch, feat))
            else:
                batch = feat
            if batch.shape[0] >= params['n_pca_dims']:
                pca.partial_fit(batch)
                batch = None

            if (i+1) in checkpoints:
                now = datetime.datetime.now()
                print('{} - {}/{} ({:.2f}%)'.format(now, i+1, N, ((i+1) * 100) / N))

        print('Saving PCA model...')
        basedir = os.path.dirname(model_filename)
        if not os.path.exists(basedir):
            os.makedirs(basedir)
        with open(model_filename, 'wb') as pca_file:
            pickle.dump(pca, pca_file)
    else:
        print('Loading PCA model...')
        with open(model_filename, 'rb') as pca_file:
            pca = pickle.load(pca_file)

    print('Applying PCA...')
    for i, (input_path, pca_fv_path) in enumerate(file_paths):
        pca_fv_dir = os.path.dirname(pca_fv_path)
        if not os.path.exists(pca_fv_dir):
            os.makedirs(pca_fv_dir)
        input_data = np.loadtxt(input_path, delimiter=',')
        np.savetxt(pca_fv_path, pca.transform(input_data), delimiter=',')
        if (i+1) in checkpoints:
            now = datetime.datetime.now()
            print('{} - {}/{} ({:.2f}%)'.format(now, i+1, N, ((i+1) * 100) / N))

if __name__ == '__main__':
    main()

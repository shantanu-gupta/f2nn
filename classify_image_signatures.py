#!/usr/bin/env python

""" classify_image_signatures.py
    
    The one script to evaluate all signature computation schemes.
"""

import os
import sys
import argparse
import datetime
import time
import itertools

import numpy as np

DELIMITER = ','

def match_signatures(signatures, test_fvs, metric):
    signature_fvs, signature_labels = signatures
    N = len(signature_labels)

    if metric == 'cos':
        norms_train = np.linalg.norm(signature_fvs, axis=1)
        norms_test = np.linalg.norm(test_fvs, axis=1)
    
    N_test = test_fvs.shape[0]
    for i in range(N_test):
        fv = test_fvs[i,:]

        if metric == 'l1':
            d = np.sum(np.abs(fv - signature_fvs), axis=1)
        elif metric == 'l2':
            d = np.sqrt(np.sum((fv - signature_fvs) ** 2, axis=1))
        elif metric.startswith('l'):
            # Let's hope the input is well-formed...
            frac_p = float(metric[1:])
            d = np.sum(np.abs(fv - signature_fvs) ** frac_p, axis=1)
            d = d ** (1.0 / frac_p)
        elif metric == 'cos':
            fv_norm = norms_test[i]
            c = np.sum(fv * signature_fvs, axis=1) / (fv_norm * norms_train)
            d = 1 - c
        else:
            raise NotImplementedError
        
        # Free up some memory...
        # np.delete(test_fvs, 0, axis=0)

        nearest = np.argmin(d)
        min_dist = d[nearest]
        yield (signature_labels[nearest], min_dist)

def main():
    # Help texts.
    description = ('Evaluate the given feature representation for license '
                    'plate recognition using signature matching. Signature '
                    'matching proceeds via a nearest-neighbour assignment '
                    'of a test image signature to one of the training set '
                    'signatures, along with a confidence value.')
    train_help = ('File containing the paths of the training set along with '
                    'their corresponding labels.')
    test_help = ('File containing the paths of the test set along with their '
                    'corresponding labels.')
    outdir_help = ('The directory in which to store the output.')
    output_help = ('The name to give to the output file.')
    metric_help = ('The metric to apply for computing distances.'
                    'Can be one of : cos, l1, l2, l<p> where p is a float.')
    normalise_help = ('Whether to mean-centre the data and normalise all '
                        'feature variances to 1.')

    # Creating the command-line parser.
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-s', '--signatures', nargs=1, help=train_help)
    parser.add_argument('-t', '--test', nargs=1, help=test_help)
    parser.add_argument('-d', '--output-dir', nargs=1, help=outdir_help)
    parser.add_argument('-o', '--output', nargs=1, help=output_help)
    parser.add_argument('-m', '--metric', nargs=1, default=('cos',),
                        help=metric_help)
    parser.add_argument('-n', '--normalise-features', action='store_true',
                        help=normalise_help)

    # Parsing the command-line arguments.
    args = parser.parse_args()
    signature_list_file, = args.signatures
    test_list_file, = args.test
    benchmark_dir, = args.output_dir
    output_name, = args.output
    metric, = args.metric
    normalise_features = args.normalise_features

    # Read in the signatures.
    print('Reading in the signatures...')
    with open(signature_list_file) as sig_list_file:
        sig_files, sig_labels = zip(*[l.strip().split()
                                      for l in sig_list_file.readlines()
                                      if len(l.strip()) > 0])
        N = len(sig_files)
        print('{} files to read.'.format(N))
        n_dims = np.loadtxt(sig_files[0], delimiter=DELIMITER)\
                    .reshape(1, -1).shape[1]
        print('{}-dimensional feature vectors.'.format(n_dims))
        sig_fvs = np.empty((N, n_dims))
        checkpoints = [i * (N // 100) for i in range(1, 101)]
        for i, f in enumerate(sig_files):
            sig_fvs[i,:] = np.loadtxt(f, delimiter=DELIMITER).reshape(1, -1)
            if i+1 in checkpoints:
                now = datetime.datetime.now()
                print('{} - {}/{} ({:.2f}%)'.format(now, i+1, N, ((i+1) * 100) / N))
    
    # Read in the test data.
    print('Reading in the test data...')
    with open(test_list_file) as list_file:
        test_files, test_labels = zip(*[l.strip().split()
                                        for l in list_file.readlines() 
                                        if len(l.strip()) > 0])
        N = len(test_files)
        print('{} files to read.'.format(N))
        n_dims = np.loadtxt(sig_files[0], delimiter=DELIMITER)\
                    .reshape(1, -1).shape[1]
        print('{}-dimensional feature vectors.'.format(n_dims))
        test_fvs = np.empty((N, n_dims))
        checkpoints = [i * (N // 100) for i in range(1, 101)]
        for i, f in enumerate(test_files):
            test_fvs[i,:] = np.loadtxt(f, delimiter=DELIMITER).reshape(1, -1)
            if i+1 in checkpoints:
                now = datetime.datetime.now()
                print('{} - {}/{} ({:.2f}%)'.format(now, i+1, N, ((i+1) * 100) / N))

    assert test_fvs.shape[1] == sig_fvs.shape[1]
    n_dims = sig_fvs.shape[1]
    n_lps = sig_fvs.shape[0]
    n_test = test_fvs.shape[0]

    # Mean-centre and normalise the variance of each feature vector to 1.
    if normalise_features:
        print('Normalising feature variances to 1...')
        mean_signature = np.mean(sig_fvs, axis=0).reshape((1, n_dims))
        sig_fvs -= mean_signature

        # training_examples are already mean-centred now. So all values are
        # deviations.
        stdev_signature = np.linalg.norm(sig_fvs, axis=0)\
                            .reshape((1, n_dims))
        stdev_signature /= np.sqrt(n_lps)

        # should force bad components to 0 rather than blowing up to infinity.
        stdev_signature[stdev_signature == 0.0] = 1.0e15
        sig_fvs /= stdev_signature

        # Do the same normalisation for the test set as well.
        test_fvs -= mean_signature
        test_fvs /= stdev_signature

    # Run the nearest-neighbour matching.
    print('Running nearest-neighbour matching with {} metric...'.format(metric))
    matches = match_signatures((sig_fvs, sig_labels),
                               test_fvs, metric)

    # Set up the output
    if os.path.isabs(output_name) or \
            os.path.join(benchmark_dir, 'classify_output') in output_name:
        # can expect output_name to be a sensible file path I guess..
        output_filename = output_name
    else:
        output_dir = os.path.join(benchmark_dir, 'classify_output')
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        output_filename = os.path.join(output_dir,
                                       '%s_%d.csv' % (output_name,
                                                      int(time.time())))
    
    # Look at the matching.
    print('Outputting matching results...')
    checkpoints = [i * (n_test // 100) for i in range(1, 101)]
    if checkpoints[-1] != n_test:
        checkpoints.append(n_test)
    with open(output_filename, 'w') as output:
        n_processed = 0
        n_correct = 0
        output.write('assigned,min_dist,true_label\n')
        for match, true_label in zip(matches, test_labels):
            assigned, min_dist = match
            output.write(','.join([assigned, str(min_dist), true_label]))
            output.write('\n')

            n_processed += 1
            if assigned == true_label:
                n_correct += 1

            if n_processed in checkpoints:
                now = datetime.datetime.now()
                print('{} {}/{} ({:.2f}%) processed. Overall accuracy {:.2f}%'
                        .format(now, n_processed, n_test,
                                (100 * n_processed) / n_test,
                                (100 * n_correct) / n_processed))
    print('Done.')

    # Accuracy output.
    accuracy = n_correct / n_test
    print('Accuracy : {:.4f}'.format(accuracy))

if __name__ == '__main__':
    main()

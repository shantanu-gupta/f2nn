""" wavelet_feature_extractor.py
    
    Finds the multilevel wavelet decomposition of the given image and returns
    its wavelet transform coefficients into a single array.
"""

import numpy as np
import pywt

from src.feature_extractors._base import FeatureExtractor
from src.feature_extractors.common_io import get_image, get_sig

class WaveletFeatureExtractor(FeatureExtractor):
    def __init__(self, args):
        self.wavelet_type = args.wavelet

    def get_signature(self, path):
        img = get_image(path)
        W = pywt.wavedec2(img, self.wavelet_type)
        coeffs = []
        coeffs.extend(W[0].flatten())
        for Wi in W[1:]:
            for w in Wi:
                coeffs.extend(w.flatten())
        return np.array(coeffs).reshape(1, -1)

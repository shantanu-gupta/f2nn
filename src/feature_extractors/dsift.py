""" run_dsift.py
    
    Uses VLFeat to compute a DSIFT representation of the given image.

    The DSIFT representation is a dense multi-scale SIFT representation computed
    over a grid overlaid on the image (at different scales), finally yielding a
    bunch of SIFT descriptors from each grid point at each scale, all laid out
    in an N_POINTS x N_SIFT_DIMS array
"""

import numpy as np
import subprocess
import tempfile

from src.feature_extractors._base import FeatureExtractor

class DSIFTFeatureExtractor(FeatureExtractor):
    def __init__(self, args):
        self.params_file = args.params_file
        self.dsift_bin_path = args.dsift_bin

    def get_signature(self, path):
        TEMP_DIR = '/dev/shm'
        temp_file = tempfile.NamedTemporaryFile(dir=TEMP_DIR)
        temp_filepath = temp_file.name
        subprocess.call([self.dsift_bin_path, path, temp_filepath,
                         self.params_file])
        # return should close temp_file automatically
        return np.loadtxt(temp_filepath, delimiter=',')

    def get_signatures_batched(self, paths):
        raise NotImplementedError

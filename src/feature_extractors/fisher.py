""" fisher.py
    
    Uses VLFeat to compute a Fisher vector encoding of an image using a
    specified GMM vocabulary.
"""

import subprocess
import tempfile
import os
import numpy as np

from src.feature_extractors._base import FeatureExtractor

class FisherFeatureExtractor(FeatureExtractor):
    def __init__(self, args):
        self.params_file = args.params_file
        self.fisher_bin_path = args.fisher_bin
        self.gmm_means = os.path.join(args.gmm_dir, 'means.csv')
        self.gmm_covs = os.path.join(args.gmm_dir, 'covs.csv')
        self.gmm_priors = os.path.join(args.gmm_dir, 'priors.csv')

    def get_signature(self, path):
        TEMP_DIR = '/dev/shm'
        temp_file = tempfile.NamedTemporaryFile(dir=TEMP_DIR)
        temp_filepath = temp_file.name
        subprocess.call([self.fisher_bin_path, path, self.params_file,
                         self.gmm_means, self.gmm_covs, self.gmm_priors,
                         temp_filepath])
        # return should close temp file automatically
        return np.loadtxt(temp_filepath, delimiter=',')

""" _base.py
    
    Base feature extractor

    Just provides a default method of batching signature computations.
    Signature computations are of course left to subclasses.
    Subclasses can provide more efficient implementations if they want.
"""
import numpy as np

class FeatureExtractor(object):
    def __init__(self, args):
        pass

    def get_signature(self, path):
        raise NotImplementedError

    def get_signatures_batched(self, paths):
        return [self.get_signature(path) for path in paths]

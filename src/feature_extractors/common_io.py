""" common_io.py
    
    I/O functions common to all feature extractors.
"""

import numpy as np
from scipy.misc import imread

def get_image(path):
    return imread(path, flatten=True) / 255.0

def get_sig(path):
    with open(path.strip(), 'rb') as sig_f:
        return np.fromfile(sig_f, np.float32)[4:].flatten()

""" raw_feature_extractor.py
    
    Pretty self-explanatory.
"""

from src.feature_extractors._base import FeatureExtractor
from src.feature_extractors.common_io import get_image, get_sig

class RawFeatureExtractor(FeatureExtractor):
    def get_signature(self, path):
        return get_image(path).flatten().reshape(1, -1)

class RawSigFeatureExtractor(FeatureExtractor):
    def get_signature(self, path):
        return get_sig(path).flatten().reshape(1, -1)

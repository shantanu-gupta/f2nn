""" caffe_feature_extractor.py
"""

import os
import sys

import numpy as np
import caffe

from src.feature_extractors._base import FeatureExtractor
from src.feature_extractors.common_io import get_image, get_sig

"""
    For extracting features from images (2D or 3D) using Caffe.
"""
class CaffeFeatureExtractor(FeatureExtractor):
    def __init__(self, args):
        self.model_prototxt = args.proto
        self.model_trained = args.weights
        self.layer_name = args.layer
        self.mean_img_path = args.mean_path
        self.image_size = args.size
        self.scale, = args.scale
        self.n_channels = 1 if args.grayscale else 3

        caffe.set_device(0)
        caffe.set_mode_gpu()

        print('Loading net from {} and {}...'.format(self.model_prototxt,
                                                     self.model_trained))

        self.mean_img = np.load(self.mean_img_path)

        if len(self.mean_img.shape) == 3: # is already a 3-channel image
            mean = self.mean_img
            channel_swap = None # TODO: look into this
        else:
            if self.n_channels == 3:
                mean = np.hstack((np.mean(self.mean_img),) * 3) # make 3-channel
                channel_swap = (2,1,0)
            else:
                mean = np.array([np.mean(self.mean_img)])
                channel_swap = None
            
        self.net = caffe.Classifier(self.model_prototxt,
                                    self.model_trained,
                                    mean=mean,
                                    channel_swap=channel_swap,
                                    raw_scale=self.scale,
                                    image_dims=self.image_size)

        layers = [k for k, v in self.net.blobs.items()]
        assert self.layer_name in layers, "Layer not found in the net!"

    def get_signature(self, path):
        img = get_image(img_path)
        p_img = img[:,:,np.newaxis]
        if self.n_channels == 3:
            p_img = np.tile(p_img, (1,1,3))

        prediction = self.net.predict([p_img], oversample=False)
        return self.net.blobs[self.layer_name].data[0].reshape(1, -1)

    def get_signatures_batched(self, paths):
        imgs = np.array([get_image(path) for path in img_paths])
        p_imgs = imgs[:,:,:,np.newaxis]
        if self.n_channels == 3:
            p_imgs = np.tile(p_imgs, (1,1,1,3))
            
        if self.net.blobs['data'].shape[0] != p_imgs.shape[0]:
            self.net.blobs['data'].reshape(p_imgs.shape[0], # batch_size
                                           p_imgs.shape[3], # n_channels
                                           # height and width
                                           self.net.blobs['data'].data.shape[2],
                                           self.net.blobs['data'].data.shape[3])
            self.net.reshape()
        predictions = self.net.predict(p_imgs, oversample=False)
        return self.net.blobs[self.layer_name].data

"""
    For extracting features from sig files (1D file containing one vector) using
    Caffe.
"""
class CaffeSigFeatureExtractor(FeatureExtractor):
    def __init__(self, args):
        self.model_prototxt = args.proto
        self.model_trained = args.weights
        self.layer_name = args.layer
        print('Loading net from {} and {}...'.format(self.model_prototxt,
                                                     self.model_trained))
        caffe.set_mode_gpu()
        self.net = caffe.Net(self.model_prototxt,self.model_trained,caffe.TEST)
        layers = [k for k, v in self.net.blobs.items()]
        assert self.layer_name in layers, "Layer not found in the net!"

    def get_signature(self, path):
        sample = get_sig(path)
        p_sample = sample[:,np.newaxis, np.newaxis]
        # prediction = self.net.forward_all([p_sample])
        self.net.blobs['data'].data[...] = np.asarray([p_sample])
        self.net.forward()
        return self.net.blobs[self.layer_name].data[0].reshape(1, -1)

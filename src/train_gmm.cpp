#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/ml.hpp>

extern "C" {
	#include <vl/gmm.h>
}

#include <nlohmann/json.hpp>
using json = nlohmann::json;

#include <fstream>
#include <iostream>
#include <vector>

struct Fisher_Parameters {
	public:
	int N_PCA_DIMS;
	int N_GMM_COMPONENTS;

	void initialize_from_file(std::string filename) {
		std::ifstream file(filename);
		json object;
		file >> object;
		N_PCA_DIMS = object["n_pca_dims"];
		N_GMM_COMPONENTS = object["n_gmm_components"];
	}
};

std::vector<std::string> read_filelist(std::string filelist_file) {
	std::ifstream file(filelist_file, std::ios::in);
	std::vector<std::string> filenames;
	for (std::string line; std::getline(file, line); ) {
		filenames.push_back(line);
	}
	
	return filenames;
}

Fisher_Parameters FISHER_PARAMS;

int main(int argc, char *argv[]) {
	if (argc == 6) {
		std::string csv_filelist_file(argv[1]);
		std::string fisher_params_file(argv[2]);
		std::string gmm_means_filename(argv[3]);
		std::string gmm_covs_filename(argv[4]);
		std::string gmm_priors_filename(argv[5]);
		FISHER_PARAMS.initialize_from_file(fisher_params_file);

		std::vector<cv::Mat> csv_mats;
		for (auto filename : read_filelist(csv_filelist_file)) {
			csv_mats.push_back(cv::ml::TrainData::loadFromCSV(
									filename, 0, -2, 0)->getTrainSamples());
		}

		cv::Mat BFM;
		cv::vconcat(csv_mats, BFM); // BFM should be N_samples x N_DIMS now
									// in row-major order
									// VLFeat wants N_DIMS x N_samples
									// but in column-major order, which works
									// out to be the same thing, so we should be
									// good...
		BFM.convertTo(BFM, CV_32F);	// want to ensure it's float
		if (BFM.isContinuous()) {
			std::cout << "Yay!\n";
			
			const int N_SAMPLES = BFM.rows;
			VlGMM *gmm = vl_gmm_new(VL_TYPE_FLOAT,
									FISHER_PARAMS.N_PCA_DIMS,
									FISHER_PARAMS.N_GMM_COMPONENTS);
			vl_gmm_set_max_num_iterations(gmm, 100);
			vl_gmm_set_initialization(gmm, VlGMMKMeans);
			vl_gmm_set_verbosity(gmm, 2);
			vl_gmm_cluster(gmm, (float*) BFM.data, N_SAMPLES);

			// Assuming the same layout as for the data, VLFeat should keep a
			// N_DIMS x N_COMPONENTS array for means, in column-major order.
			// This is again the same as an OpenCV row-major mat that is
			// N_COMPONENTS x N_DIMS, so we should again be good...?
			cv::Mat gmm_means(FISHER_PARAMS.N_GMM_COMPONENTS, 
							  FISHER_PARAMS.N_PCA_DIMS,
							  CV_32F, (float*) vl_gmm_get_means(gmm));
			cv::Mat gmm_covs(FISHER_PARAMS.N_GMM_COMPONENTS,
							 FISHER_PARAMS.N_PCA_DIMS,
							 CV_32F, (float*) vl_gmm_get_covariances(gmm));
			cv::Mat gmm_priors(FISHER_PARAMS.N_GMM_COMPONENTS, 1, CV_32F,
								(float*) vl_gmm_get_priors(gmm));

			std::ofstream gmm_means_file(gmm_means_filename);
			gmm_means_file << cv::format(gmm_means, cv::Formatter::FMT_CSV);
			gmm_means_file.close();

			std::ofstream gmm_covs_file(gmm_covs_filename);
			gmm_covs_file << cv::format(gmm_covs, cv::Formatter::FMT_CSV);
			gmm_covs_file.close();

			std::ofstream gmm_priors_file(gmm_priors_filename);
			gmm_priors_file << cv::format(gmm_priors, cv::Formatter::FMT_CSV);
			gmm_priors_file.close();

			std::cout << "Done!\n";
		}
	} else {
		std::cerr << "Not enough arguments!\n";
	}

	return 0;
}

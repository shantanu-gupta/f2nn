#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>

extern "C" {
	#include <vl/dsift.h>
}

#include <nlohmann/json.hpp>
using json = nlohmann::json;

#include <fstream>
#include <iostream>
#include <string>
#include <map>

/*
 * DSIFT geometry parameters:
 *
 *	N_SCALES : will downsample the image this many times and recompute DSIFT at
 *			   each scale generated
 *	DSIFT_STEP : spatial sampling interval in both vertical and horizontal
 *				 directions
 *	DSIFT_BINSIZE : spatial binsize (in px), in both vertical and horizontal
 *					directions; there will be 4x4 spatial bins in total by
 *					default
 */
struct SIFT_Parameters {
	public:
	int N_SCALES;
	int DSIFT_STEP;
	int DSIFT_BINSIZE; // spatial binsize

	void initialize_from_file(std::string filename) {
		std::ifstream file(filename);
		json object;
		file >> object;
		N_SCALES = object["n_scales"];
		DSIFT_STEP = object["dsift_step"];
		DSIFT_BINSIZE = object["dsift_binsize"];
	}
};

SIFT_Parameters SIFT_PARAMS;

std::vector<float> get_pixel_array(cv::Mat img) {
	// assume img is grayscale
	std::vector<float> pixels;
	for (int i = 0; i < img.rows; ++i) {
		for (int j = 0; j < img.cols; ++j) {
			pixels.push_back(img.at<unsigned char>(i,j) / 255.0f);
		}
	}
	return pixels;
}

cv::Mat get_dsift_descriptor(cv::Mat img) {
	std::vector<float> pixels = get_pixel_array(img);

	VlDsiftFilter* dsift = vl_dsift_new_basic(img.cols, img.rows,
												SIFT_PARAMS.DSIFT_STEP,
												SIFT_PARAMS.DSIFT_BINSIZE);
	vl_dsift_process(dsift, &pixels[0]);

	int n_kp = vl_dsift_get_keypoint_num(dsift);
	int dsize = vl_dsift_get_descriptor_size(dsift);
	const float* descriptors = vl_dsift_get_descriptors(dsift);
	cv::Mat dsift_descriptor(n_kp, dsize, CV_32F);
	for (int i = 0; i < n_kp; ++i) {
		for (int j = 0; j < dsize; ++j) {
			dsift_descriptor.at<float>(i, j) = descriptors[i*dsize + j];
		}
	}
	return dsift_descriptor;
}

int main(int argc, char *argv[]) {
	std::string img_filename(argv[1]);
	std::string out_filename(argv[2]);
	std::string params_filename(argv[3]);
	SIFT_PARAMS.initialize_from_file(params_filename);

	cv::Mat img = cv::imread(img_filename, cv::IMREAD_GRAYSCALE);
	cv::Mat descriptors = get_dsift_descriptor(img);
	for (int i = 1; i < SIFT_PARAMS.N_SCALES; ++i) {
		// i == 0 already covered in initialization
		cv::pyrDown(img, img);
		cv::vconcat(descriptors, get_dsift_descriptor(img), descriptors);
	}

	std::ofstream out_file(out_filename, std::ios::binary);
	out_file << cv::format(descriptors, cv::Formatter::FMT_CSV);

	return 0;
}

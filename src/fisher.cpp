/*
 * See src/dsift.cpp and src/train_gmm.cpp for proper explanations of data
 * format and layout.
 */

#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/ml.hpp>

extern "C" {
	#include <vl/fisher.h>
}

#include <nlohmann/json.hpp>
using json = nlohmann::json;

#include <fstream>
#include <iostream>
#include <vector>

struct Fisher_Parameters {
	public:
	int N_PCA_DIMS;
	int N_GMM_COMPONENTS;

	void initialize_from_file(std::string filename) {
		std::ifstream file(filename);
		json object;
		file >> object;
		N_PCA_DIMS = object["n_pca_dims"];
		N_GMM_COMPONENTS = object["n_gmm_components"];
	}
};

Fisher_Parameters FISHER_PARAMS;

int main(int argc, char *argv[]) {
	const int N_ARGS = 7;
	if (argc == N_ARGS) {
		std::string input_filename(argv[1]);
		std::string fisher_params_file(argv[2]);
		std::string gmm_means_filename(argv[3]);
		std::string gmm_covs_filename(argv[4]);
		std::string gmm_priors_filename(argv[5]);
		std::string output_filename(argv[6]);

		FISHER_PARAMS.initialize_from_file(fisher_params_file);

		cv::Mat input \
			= cv::ml::TrainData::loadFromCSV(
							input_filename, 0, -2, 0)->getTrainSamples();
		cv::Mat gmm_means = cv::ml::TrainData::loadFromCSV(
							gmm_means_filename, 0, -2, 0)->getTrainSamples();
		cv::Mat gmm_covs = cv::ml::TrainData::loadFromCSV(
							gmm_covs_filename, 0, -2, 0)->getTrainSamples();
		cv::Mat gmm_priors = cv::ml::TrainData::loadFromCSV(
							gmm_priors_filename, 0, -2, 0)->getTrainSamples();

		input.convertTo(input, CV_32F);
		gmm_means.convertTo(gmm_means, CV_32F);
		gmm_covs.convertTo(gmm_covs, CV_32F);
		gmm_priors.convertTo(gmm_priors, CV_32F);

		if (input.isContinuous()
				&& gmm_means.isContinuous()
				&& gmm_covs.isContinuous()
				&& gmm_priors.isContinuous()) {
			float* encoding = new float[2 * FISHER_PARAMS.N_GMM_COMPONENTS
										  * FISHER_PARAMS.N_PCA_DIMS];
			vl_fisher_encode(encoding, VL_TYPE_FLOAT, 
								(float*) gmm_means.data,
								FISHER_PARAMS.N_PCA_DIMS,
								FISHER_PARAMS.N_GMM_COMPONENTS,
								(float*) gmm_covs.data,
								(float*) gmm_priors.data,
								(float*) input.data,
								input.rows,
								VL_FISHER_FLAG_IMPROVED);
			cv::Mat encoding_mat(1, 
								 2 * FISHER_PARAMS.N_GMM_COMPONENTS
								   * FISHER_PARAMS.N_PCA_DIMS,
								 CV_32F, encoding);
			std::ofstream output_file(output_filename);
			output_file << cv::format(encoding_mat, cv::Formatter::FMT_CSV);
			output_file.close();
		} else {
			std::cerr << "Mat not continuous!\n";
		}
	} else if (argc < N_ARGS) {
		std::cerr << "Not enough arguments!\n";
	} else {
		std::cerr << "Too many arguments!\n";
	}

	return 0;
}

#!/usr/bin/env python
""" extract_signatures.py
    
    Feature extractor which outputs signature feature vectors for each image
    file given as input.

    Acts as a gateway to all other feature extractors.
"""

import argparse
import datetime

import numpy as np
import os
import concurrent.futures

feature_types = ['raw', 'raw_sig', 'wavelet', 'caffe', 'caffe_sig',
                 'dsift', 'fisher']

def load_feature_extractors():
    # Separate function because loading caffe takes a while, and the
    # argcomplete module will make that happen lots of times.
    from src.feature_extractors.raw import RawFeatureExtractor
    from src.feature_extractors.raw import RawSigFeatureExtractor
    from src.feature_extractors.wavelet import WaveletFeatureExtractor
    from src.feature_extractors.caffe import CaffeFeatureExtractor
    from src.feature_extractors.caffe import CaffeSigFeatureExtractor
    from src.feature_extractors.dsift import DSIFTFeatureExtractor
    from src.feature_extractors.fisher import FisherFeatureExtractor

    feature_extractors = {'raw': RawFeatureExtractor,
                          'raw_sig': RawSigFeatureExtractor,
                          'wavelet': WaveletFeatureExtractor,
                          'caffe': CaffeFeatureExtractor,
                          'caffe_sig': CaffeSigFeatureExtractor,
                          'dsift': DSIFTFeatureExtractor,
                          'fisher': FisherFeatureExtractor}
    return feature_extractors

def add_feature_extractor_parsers(parser):
    subparsers_help = ('Select one of the implemented feature extractors.')
    subparsers = parser.add_subparsers(dest='method', help=subparsers_help)
    
    # Parser for raw features.
    raw_help = ('Just flattens all pixel values into an array.')
    parser_r = subparsers.add_parser('raw', help=raw_help)

    # Parser for raw sig features.
    raw_sig_help = ('Reads the sig file and returns it as a flat vector.')
    parser_rs = subparsers.add_parser('raw_sig', help=raw_sig_help)

    # Parser for wavelet features.
    wavelet_help = ('Uses PyWavelets to compute a wavelet decomposition. '
                    'Returns all coefficients in a flat array.')
    wavelet_type_help = ('The wavelet type to use.')
    parser_w = subparsers.add_parser('wavelet', help=wavelet_help)
    parser_w.add_argument('wavelet', metavar='wavelet-type', type=str,
                          help=wavelet_type_help)
    
    # Parser for caffe features.
    caffe_help = ('Uses Caffe to compute convolutional net features.')
    proto_help = ('The prototxt file for the Caffe model.')
    model_help = ('The pretrained network weights for the model.')
    layer_help = ('The layer to extract the features from.')
    size_help = ('The size of the input images.')
    scale_help = ('The scale of the pixel values in the input images.')
    gray_help = ('Whether the input images are grayscale (single channel).')
    mean_img_path_help = ('The location of the mean image of the training set.')
    parser_c = subparsers.add_parser('caffe', help=caffe_help)
    parser_c.add_argument('proto', type=str, help=proto_help)
    parser_c.add_argument('weights', type=str, help=model_help)
    parser_c.add_argument('layer', type=str, help=layer_help)
    parser_c.add_argument('mean_path', type=str, help=mean_img_path_help)
    parser_c.add_argument('--size', type=int, nargs=2, default=(256,256),
                          help=size_help)
    parser_c.add_argument('--scale', nargs=1, default=(255.0,), type=float,
                          help=scale_help)
    parser_c.add_argument('--grayscale', action='store_true', default=False,
                          help=gray_help)

    # Parser for flat input data. Has a subset of the parameters of the
    # CaffeFeatureExtractor.
    caffe_sig_help = ('Uses Caffe to run a vector through some FC layers.')
    parser_cf = subparsers.add_parser('caffe_sig', help=caffe_sig_help)
    parser_cf.add_argument('proto', type=str, help=proto_help)
    parser_cf.add_argument('weights', type=str, help=model_help)
    parser_cf.add_argument('layer', type=str, help=layer_help)

    # Parser for DSIFT features.
    dsift_help = ('Computes dense SIFT using VLFeat.')
    dsift_bin_help = ('Location of the dsift binary.')
    params_file_help = ('Location of the json file holding hyperparameters to '
                        'pass to the dsift binary.')
    parser_d = subparsers.add_parser('dsift', help=dsift_help)
    parser_d.add_argument('dsift_bin', type=str, help=dsift_bin_help)
    parser_d.add_argument('params_file', type=str, help=params_file_help)

    # Parser for Fisher vector features
    fisher_help = ('Computes the Fisher vector using VLFeat.')
    fisher_bin_help = ('Location of the \'fisher\' binary.')
    params_file_help = ('Location of the json file holding hyperparameters to '
                        'pass to the fisher binary.')
    gmm_dir_help = ('Location of the directory holding the GMM vocabulary to '
                    'use for computing the Fisher encoding.')
    parser_f = subparsers.add_parser('fisher', help=fisher_help)
    parser_f.add_argument('fisher_bin', type=str, help=fisher_bin_help)
    parser_f.add_argument('params_file', type=str, help=params_file_help)
    parser_f.add_argument('gmm_dir', type=str, help=gmm_dir_help)

def get_and_write_signature(extractor, input_path, output_path):
    np.savetxt(output_path, extractor.get_signature(input_path),
               delimiter=',', fmt='%.8g')

def main():
    # Help texts
    description = ('Processes the input list of images to compute a feature '
                    'representation (signature) of each image. This signature '
                    'will be used later by classify_image_signatures.py to '
                    'assign labels to new licence plate images.')
    files_help = ('File containing a list of pairs: one pair contains a path '
                    'to a licence plate image, and a corresponding path to '
                    'which its signature should be written.')
    batch_size_help = ('How many images to pass to the feature extractor at '
                        'once. (May help with performance in Caffe)')
    parallelise_help = ('Set this flag to enable parallel computation of '
                        'signatures. May not be advisable if GPUs are '
                        'involved.')

    # Creating the command-line parser.
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-p', '--paths', nargs=1, help=files_help)
    parser.add_argument('-b', '--batchsize', type=int, nargs=1,
                        default=(1,), help=batch_size_help)
    parser.add_argument('--parallel', action='store_true', default=False,
                        help=parallelise_help)

    # Add sub-parsers for different feature extractors.
    add_feature_extractor_parsers(parser)    

    # Parsing the command-line arguments.
    print('Parsing the command-line arguments...')
    args = parser.parse_args()

    pairs_listfile, = args.paths
    batch_size, = args.batchsize
    method = args.method
    parallelise = args.parallel

    # Initialising the specified feature extractor
    print('Initialising the specified feature extractor...')
    feature_extractors = load_feature_extractors()
    extractor = feature_extractors[method](args)

    # Iterating through the input images.
    print('Iterating through the input from {}...'.format(pairs_listfile))
    with open(pairs_listfile) as inp:
        file_paths = [line.split() for line in inp if len(line.strip()) > 0]
    N = len(file_paths)
    checkpoints = [i * (N // 100) for i in range(1, 101)]
    if checkpoints[-1] != N:
        checkpoints.append(N)

    # Making sure all needed dirs exist
    for (input_path, output_path) in file_paths:
        basedir = os.path.dirname(output_path)
        if not os.path.exists(basedir):
            print('Making directory {}.'.format(basedir))
            os.makedirs(basedir)

    if batch_size == 1:
        if parallelise:
            chunksize = max(N // 100, 1)
            extractors = [extractor] * N
            input_paths, output_paths = zip(*file_paths)
            with concurrent.futures.ProcessPoolExecutor() as executor:
                i = 0
                for result in executor.map(get_and_write_signature, extractors,
                                           input_paths, output_paths,
                                           chunksize=chunksize):
                    i += 1
                    if i in checkpoints:
                        now = datetime.datetime.now()
                        print('{} - {}/{} ({:.2f}%)'
                                .format(now, i, N, (i * 100) / N))
        else:
            for i, (input_path, output_path) in enumerate(file_paths):
                signature = extractor.get_signature(input_path)
                
                basedir = os.path.dirname(output_path)
                if not os.path.exists(basedir):
                    print('Making directory {}.'.format(basedir))
                    os.makedirs(basedir)

                np.savetxt(output_path, signature, delimiter=',', fmt='%.8g')
                if (i+1) in checkpoints:
                    now = datetime.datetime.now()
                    print('{} - {}/{} ({:.2f}%)'.format(now, i+1, N,
                                                        ((i+1) * 100) / N))
    else:
        n = 0
        while n < N:
            batch_end = min(n + batch_size, N)
            inputs, outputs = zip(*file_paths[n:batch_end])
            batch_signatures = extractor.get_signatures_batched(inputs)
            for signature, output_path in zip(batch_signatures, outputs):
                np.savetxt(output_path, signature, delimiter=',', fmt='%.8g')
            n = batch_end
            if n in checkpoints:
                now = datetime.datetime.now()
                print('{} - {}/{} ({:.2f}%)'.format(now, n, N, (n * 100) / N))
    print('Done!')
                
if __name__ == '__main__':
    main()

#!/usr/bin/env python
""" get_diff_root_listfile.py
    
    Modify a listfile to replace a root directory with a different one.
    Also allows changing extensions.
"""

import os.path as path
import argparse

def replace_line(line, old_root, new_root, new_ext=None):
    new_line = line.replace(old_root, new_root)
    if new_ext is not None:
        old_ext = path.splitext(new_line)[1]
        new_line = new_line.replace(old_ext, new_ext)
    return new_line

def main():
    description = ('Get a new file containing modified paths from a listfile '
                    'given as input.')
    input_help = ('File containing paths to be copied over with new root.')
    output_help = ('The new file to be created, which will hold the modified '
                    'paths.')
    old_root_help = ('The old root directory which will be replaced in the new '
                    'file.')
    new_root_help = ('The new root directory to be put into the output list.')
    new_ext_help = ('Can also change the extension type in the output list.')

    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-f', '--listfile', nargs=1, help=input_help)
    parser.add_argument('-o', '--output', nargs=1, help=output_help)
    parser.add_argument('-s', '--old-root', nargs=1, help=old_root_help)
    parser.add_argument('-r', '--new_root', nargs=1, help=new_root_help)
    parser.add_argument('-e', '--extension', nargs=1, help=new_ext_help)

    args = parser.parse_args()

    input_listfile, = args.listfile
    output_listfile, = args.output
    old_root, = args.old_root
    new_root, = args.new_root
    new_ext = None
    if args.extension is not None:
        new_ext, = args.extension

    with open(output_listfile, 'w') as out, open(input_listfile) as inp:
        for line in inp:
            out.write(replace_line(line.strip(), old_root, new_root, new_ext) + '\n')

if __name__ == '__main__':
    main()
